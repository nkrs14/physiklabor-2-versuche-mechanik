package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

func createPythonGraph(dataAccel []point, fileAccel string, dataGPS []point, fileGPS string, filePython string) {

	if dataAccel != nil {
		f1, err := json.MarshalIndent(dataAccel, "", " ")

		if err != nil {
			log.Println("error when parsing accel data" + err.Error())
		}

		err = ioutil.WriteFile(fileAccel+".data.json", f1, 0644)

		if err != nil {
			log.Println("error when parsing accel data" + err.Error())
		}
	} else {
		log.Println("no accel data")
	}

	if dataGPS != nil {
		f2, err := json.MarshalIndent(dataGPS, "", " ")

		if err != nil {
			log.Println("error when parsing gps data" + err.Error())
		}

		err = ioutil.WriteFile(fileGPS+".data.json", f2, 0644)

		if err != nil {
			log.Println("error when parsing gps data" + err.Error())
		}
	} else {
		log.Println("no gps data")
	}

	var output string

	//import dependencies
	output += "import matplotlib.pyplot as plt\n"
	output += "import numpy as np\n"
	output += "import sys\n"
	output += "import json\n\n"

	//read files
	output += "with open('" + fileAccel + ".data.json') as f1:\n"
	output += "    dataACCEL = json.load(f1)\n\n"

	output += "with open('" + fileGPS + ".data.json') as f2:\n"
	output += "    dataGPS = json.load(f2)\n\n"

	//create acceleration arrays
	output += "x = np.linspace(0,1,len(dataACCEL))\n\n"

	output += "yx = np.linspace(0,1,len(dataACCEL))\n"
	output += "yy = np.linspace(0,1,len(dataACCEL))\n"
	output += "yg = np.linspace(0,1,len(dataACCEL))\n\n"

	output += "vx = np.linspace(0,1,len(dataACCEL))\n"
	output += "vy = np.linspace(0,1,len(dataACCEL))\n"
	output += "vg = np.linspace(0,1,len(dataACCEL))\n\n"

	output += "sx = np.linspace(0,1,len(dataACCEL))\n"
	output += "sy = np.linspace(0,1,len(dataACCEL))\n"
	output += "sg = np.linspace(0,1,len(dataACCEL))\n\n"

	//create gps arrays
	output += "b_x = np.linspace(0,1,len(dataGPS))\n\n"

	output += "b_yg = np.linspace(0,1,len(dataGPS))\n"
	output += "b_vy = np.linspace(0,1,len(dataGPS))\n"
	output += "b_yy = np.linspace(0,1,len(dataGPS))\n"
	output += "b_vg = np.linspace(0,1,len(dataGPS))\n"
	output += "b_sg = np.linspace(0,1,len(dataGPS))\n"
	output += "b_wg = np.linspace(0,1,len(dataGPS))\n"
	output += "b_hg = np.linspace(0,1,len(dataGPS))\n\n"

	//create plot
	output += "fig, ((a_a, b_a), (a_v, b_v), (a_s, b_s), (b_h, b_w)) = plt.subplots(nrows=4, ncols=2)\n"
	output += "fig.set_size_inches(11.7, 16.5 )\n\n"

	//configure axis
	output += "a_a.spines['left'].set_position('zero')\n"
	output += "a_a.spines['bottom'].set_position('zero')\n"
	output += "a_a.spines['right'].set_color('none')\n"
	output += "a_a.spines['top'].set_color('none')\n"
	output += "a_a.set_title('a-t Diagramm')\n"
	output += "a_a.set_xlabel('t [s]')\n"
	output += "a_a.set_ylabel('a [m/s^2]')\n\n"

	output += "a_v.spines['left'].set_position('zero')\n"
	output += "a_v.spines['bottom'].set_position('zero')\n"
	output += "a_v.spines['right'].set_color('none')\n"
	output += "a_v.spines['top'].set_color('none')\n"
	output += "a_v.set_title('v-t Diagramm')\n"
	output += "a_v.set_xlabel('t [s]')\n"
	output += "a_v.set_ylabel('v [m/s]')\n\n"

	output += "a_s.spines['left'].set_position('zero')\n"
	output += "a_s.spines['bottom'].set_position('zero')\n"
	output += "a_s.spines['right'].set_color('none')\n"
	output += "a_s.spines['top'].set_color('none')\n"
	output += "a_s.set_title('s-t Diagramm')\n"
	output += "a_s.set_xlabel('t [s]')\n"
	output += "a_s.set_ylabel('s [m]')\n\n"

	output += "b_a.spines['left'].set_position('zero')\n"
	output += "b_a.spines['bottom'].set_position('zero')\n"
	output += "b_a.spines['right'].set_color('none')\n"
	output += "b_a.spines['top'].set_color('none')\n"
	output += "b_a.set_title('GPS a-t Diagramm')\n"
	output += "b_a.set_xlabel('t [s]')\n"
	output += "b_a.set_ylabel('a [m/s^2]')\n\n"

	output += "b_v.spines['left'].set_position('zero')\n"
	output += "b_v.spines['bottom'].set_position('zero')\n"
	output += "b_v.spines['right'].set_color('none')\n"
	output += "b_v.spines['top'].set_color('none')\n"
	output += "b_v.set_title('GPS v-t Diagramm')\n"
	output += "b_v.set_xlabel('t [s]')\n"
	output += "b_v.set_ylabel('v [m/s]')\n\n"

	output += "b_s.spines['left'].set_position('zero')\n"
	output += "b_s.spines['bottom'].set_position('zero')\n"
	output += "b_s.spines['right'].set_color('none')\n"
	output += "b_s.spines['top'].set_color('none')\n"
	output += "b_s.set_title('GPS s-t Diagramm')\n"
	output += "b_s.set_xlabel('t [s]')\n"
	output += "b_s.set_ylabel('s [m]')\n\n"

	output += "b_h.spines['left'].set_position('zero')\n"
	output += "b_h.spines['bottom'].set_position('center')\n"
	output += "b_h.spines['right'].set_color('none')\n"
	output += "b_h.spines['top'].set_color('none')\n"
	output += "b_h.set_title('GPS h-t Diagramm')\n"
	output += "b_h.set_xlabel('t [s]')\n"
	output += "b_h.set_ylabel('h [m]')\n\n"

	output += "b_w.spines['left'].set_position('zero')\n"
	output += "b_w.spines['bottom'].set_position('zero')\n"
	output += "b_w.spines['right'].set_color('none')\n"
	output += "b_w.spines['top'].set_color('none')\n"
	output += "b_w.set_title('GPS P-t Diagramm')\n"
	output += "b_w.set_xlabel('t [s]')\n"
	output += "b_w.set_ylabel('P [kW]')\n\n"

	output += "plt.tight_layout(pad=0.001, w_pad=0.01, h_pad=0.001)\n\n"

	//get acceleration data
	output += "for i in range(len(dataACCEL)):\n"
	output += "    p = dataACCEL[i]\n"
	output += "    x[i] = p['time']\n\n"

	output += "    yx[i] = p['ax']\n"
	output += "    yy[i] = p['ay']\n"
	output += "    yg[i] = p['ages']\n\n"

	output += "    vx[i] = p['vx']\n"
	output += "    vy[i] = p['vy']\n"
	output += "    vg[i] = p['vges']\n\n"

	output += "    sx[i] = p['sx']\n"
	output += "    sy[i] = p['sy']\n"
	output += "    sg[i] = p['sges']\n\n"

	/*
		output += "    if i % 1000 == 0:\n"
		output += "        datx = np.linspace(x[i],x[i],2)\n"
		output += "        daty = np.linspace(vg[i]-p['errV'],vg[i]+p['errV'],2)\n"
		output += "        a_v.plot( datx, daty,'black')\n"
		output += "        daty = np.linspace(sg[i]-p['errS'],sg[i]+p['errS'],2)\n"
		output += "        a_s.plot( datx, daty,'black')\n\n"

		output += "    if i == len(dataACCEL)-1:\n"
		output += "        datx = np.linspace(x[i],x[i],2)\n"
		output += "        daty = np.linspace(vg[i]-p['errV'],vg[i]+p['errV'],2)\n"
		output += "        a_v.plot( datx, daty,'black', label ='Fehlerbalken')\n"
		output += "        daty = np.linspace(sg[i]-p['errS'],sg[i]+p['errS'],2)\n"
		output += "        a_s.plot( datx, daty,'black', label ='Fehlerbalken')\n\n"
	*/

	output += "    if i % 1000 == 0:\n"
	output += "        datx = np.linspace(x[i],x[i],2)\n"
	output += "        daty = np.linspace(vy[i]-p['errV'],vy[i]+p['errV'],2)\n"
	output += "        a_v.plot( datx, daty,'black')\n"
	output += "        daty = np.linspace(sy[i]-p['errS'],sy[i]+p['errS'],2)\n"
	output += "        a_s.plot( datx, daty,'black')\n\n"

	output += "    if i == len(dataACCEL)-1:\n"
	output += "        datx = np.linspace(x[i],x[i],2)\n"
	output += "        daty = np.linspace(vy[i]-p['errV'],vy[i]+p['errV'],2)\n"
	output += "        a_v.plot( datx, daty,'black', label ='Fehlerbalken')\n"
	output += "        daty = np.linspace(sy[i]-p['errS'],sy[i]+p['errS'],2)\n"
	output += "        a_s.plot( datx, daty,'black', label ='Fehlerbalken')\n\n"

	//get gps data
	output += "for i in range(len(dataGPS)):\n"
	output += "    p = dataGPS[i]\n"
	output += "    b_x[i] = p['time']\n"
	output += "    b_yg[i] = p['ages']\n"
	output += "    b_vy[i] = p['vy']\n"
	output += "    b_yy[i] = p['ay']\n"
	output += "    b_vg[i] = p['vges']\n"
	output += "    b_sg[i] = p['sges']\n"
	output += "    b_wg[i] = p['power']/1000.0\n"
	output += "    b_hg[i] = p['height']\n\n"

	output += "    if i % 15 == 0:\n"
	output += "        datx = np.linspace(b_x[i],b_x[i],2)\n"
	output += "        daty = np.linspace(b_yg[i]-p['errA'],b_yg[i]+p['errA'],2)\n"
	output += "        b_a.plot( datx, daty,'black')\n"
	output += "        daty = np.linspace(b_vg[i]-p['errV'],b_vg[i]+p['errV'],2)\n"
	output += "        b_v.plot( datx, daty,'black')\n"
	output += "        daty = np.linspace(b_hg[i]-p['errH'],b_hg[i]+p['errH'],2)\n"
	output += "        b_h.plot( datx, daty,'black')\n"
	output += "        daty = np.linspace(b_wg[i]-(p['errP']/1000.0),b_wg[i]+(p['errP']/1000.0),2)\n"
	output += "        b_w.plot( datx, daty,'black')\n"

	output += "        daty = np.linspace(b_sg[i]-p['errS'],b_sg[i]+p['errS'],2)\n"
	output += "        b_s.plot( datx, daty,'black')\n\n"
	output += "    if i == len(dataGPS)-1:\n"
	output += "        datx = np.linspace(b_x[i],b_x[i],2)\n"
	output += "        daty = np.linspace(b_yg[i]-p['errA'],b_yg[i]+p['errA'],2)\n"
	output += "        b_a.plot( datx, daty,'black', label ='Fehlerbalken')\n"
	output += "        daty = np.linspace(b_vg[i]-p['errV'],b_vg[i]+p['errV'],2)\n"
	output += "        b_v.plot( datx, daty,'black', label ='Fehlerbalken')\n"
	output += "        daty = np.linspace(b_hg[i]-p['errH'],b_hg[i]+p['errH'],2)\n"
	output += "        b_h.plot( datx, daty,'black', label ='Fehlerbalken')\n"
	output += "        daty = np.linspace(b_wg[i]-(p['errP']/1000.0),b_wg[i]+(p['errP']/1000.0),2)\n"
	output += "        b_w.plot( datx, daty,'black', label ='Fehlerbalken')\n"
	output += "        daty = np.linspace(b_sg[i]-p['errS'],b_sg[i]+p['errS'],2)\n"
	output += "        b_s.plot( datx, daty,'black', label ='Fehlerbalken')\n\n"

	//plot acceleration
	/*
		output += "a_a.plot(x,yx, 'r--', label ='x-Richtung')\n"
		output += "a_a.plot(x,yy, 'g--', label ='y-Richtung')\n"
		output += "a_a.plot(x,yg, 'b', label ='Summe')\n\n"

		output += "a_v.plot(x,vx, 'r--', label ='x-Richtung')\n"
		output += "a_v.plot(x,vy, 'g--', label ='y-Richtung')\n"
		output += "a_v.plot(x,vg, 'b', label ='Summe')\n\n"

		output += "a_s.plot(x,sx, 'r--', label ='x-Richtung')\n"
		output += "a_s.plot(x,sy, 'g--', label ='y-Richtung')\n"
		output += "a_s.plot(x,sg, 'b', label ='Summe')\n\n"
	*/
	output += "a_a.plot(x,yy, 'b', label ='y-Richtung')\n"
	output += "a_v.plot(x,vy, 'b', label ='y-Richtung')\n"
	output += "a_s.plot(x,sy, 'b', label ='y-Richtung')\n"

	//plot gps
	output += "b_a.plot(b_x,b_yg, 'b', label ='Rechnung')\n"
	output += "b_v.plot(b_x,b_vg, 'b', label ='Rechnung')\n"
	output += "b_v.plot(b_x,b_vy, 'r--', label ='GPS v')\n"
	output += "b_a.plot(b_x,b_yy, 'r--', label ='GPS a')\n"
	output += "b_s.plot(b_x,b_sg, 'b', label ='Rechnung')\n"
	output += "b_w.plot(b_x,b_wg, 'b', label ='Rechnung')\n"
	output += "b_h.plot(b_x,b_hg, 'b', label ='Rechnung')\n\n"

	//add ledgend
	output += "a_a.legend(loc =\"upper right\")\n"
	output += "a_v.legend(loc =\"upper right\")\n"
	output += "a_s.legend(loc =\"right\")\n"
	output += "b_a.legend(loc =\"upper right\")\n"
	output += "b_v.legend(loc =\"upper right\")\n"
	output += "b_s.legend(loc =\"right\")\n"
	output += "b_h.legend(loc =\"lower right\")\n"
	output += "b_w.legend(loc =\"upper right\")\n\n"

	output += "plt.savefig('" + filePython + ".png', dpi=300)\n"
	output += "plt.show()\n"

	//output file
	files, err := os.Create(filePython) // For read access.
	if err != nil {
		log.Fatal(err)
	}
	_, err = files.WriteString(output)
	if err != nil {
		log.Fatal(err)
	}
}

package main

import (
	"errors"
	"log"
	"math"
)

var deltaA = 0.01

func calibrateAccel(file string) (correction [3]float64, err error) {
	data, err := readCSVFile(file, 5)
	if err != nil {
		log.Fatal(err)
	}

	var maxX, maxY, maxZ float64
	var correctedX = make([]float64, len(data))
	var correctedY = make([]float64, len(data))
	var correctedZ = make([]float64, len(data))
	var dat = make([][3]float64, len(data))

	for i := 1; i < len(data); i++ {
		tmp, err := parseFloat(data[i][1])
		if err != nil {
			return [3]float64{}, err
		}
		dat[i][0] = math.Abs(tmp)

		tmp, err = parseFloat(data[i][2])
		if err != nil {
			return [3]float64{}, err
		}
		dat[i][1] = math.Abs(tmp)

		tmp, err = parseFloat(data[i][3])
		if err != nil {
			return [3]float64{}, err
		}
		dat[i][2] = math.Abs(tmp)

	}

	for j := 1; j < len(data); j++ {
		interval := 30
		if j < interval {
			sumx := 0.0
			sumy := 0.0
			sumz := 0.0

			for k := 1; k < j+interval; k++ {
				sumx += dat[k][0]
				sumy += dat[k][1]
				sumz += dat[k][2]
			}

			correctedX[j] = sumx / float64(j+interval)
			correctedY[j] = sumy / float64(j+interval)
			correctedZ[j] = sumz / float64(j+interval)

		} else if j >= len(data)-interval {
			sumx := 0.0
			sumy := 0.0
			sumz := 0.0

			for k := j - interval; k < len(data); k++ {
				sumx += dat[k][0]
				sumy += dat[k][1]
				sumz += dat[k][2]
			}

			correctedX[j] = sumx / float64(len(data)-j+interval)
			correctedY[j] = sumy / float64(len(data)-j+interval)
			correctedZ[j] = sumz / float64(len(data)-j+interval)
		} else {
			sumx := 0.0
			sumy := 0.0
			sumz := 0.0

			for k := j - interval; k < j+interval; k++ {
				sumx += dat[k][0]
				sumy += dat[k][1]
				sumz += dat[k][2]
			}

			correctedX[j] = sumx / float64(interval*2)
			correctedY[j] = sumy / float64(interval*2)
			correctedZ[j] = sumz / float64(interval*2)
		}

		if correctedX[j] > maxX {
			maxX = correctedX[j]
		}
		if correctedY[j] > maxY {
			maxY = correctedY[j]
		}
		if correctedZ[j] > maxZ {
			maxZ = correctedZ[j]
		}

	}

	return [3]float64{maxX, maxY, maxZ}, nil

}

func parseCSVDataAccel(data [][]string, offset [3]float64, startingTime float64, endTime float64) (parsedData []point, err error) {
	var firstIndex int
	var lastIndex int
	var correctedX []float64
	var correctedY []float64
	var correctedZ []float64

	//find out where the wanted data is
	for i := 1; i < len(data); i++ {
		time, err := parseFloat(data[i][0])
		if err != nil {
			return nil, err
		}

		if time < startingTime {
			continue
		} else if firstIndex == 0 {
			firstIndex = i
		} else if time > endTime {
			lastIndex = i
			parsedData = make([]point, lastIndex-firstIndex)

			correctedX = make([]float64, lastIndex-firstIndex)
			correctedY = make([]float64, lastIndex-firstIndex)
			correctedZ = make([]float64, lastIndex-firstIndex)
			break
		}
	}

	//if no start or end found exit
	if firstIndex == 0 || lastIndex == 0 {
		return nil, errors.New("index not set correctly")
	}

	//actually parse the data
	for j := 0; j < len(parsedData); j++ {

		//parse the csv data
		parsedData[j].Time, err = parseFloat(data[j+firstIndex][0])
		if err != nil {
			return nil, err
		}
		parsedData[j].Time -= startingTime

		parsedData[j].Ax, err = parseFloat(data[j+firstIndex][1])
		parsedData[j].Ax *= 9.81 / offset[0]
		if err != nil {
			return nil, err
		}

		parsedData[j].Ay, err = parseFloat(data[j+firstIndex][2])
		parsedData[j].Ay *= 9.81 / offset[1]
		if err != nil {
			return nil, err
		}

		parsedData[j].Az, err = parseFloat(data[j+firstIndex][3])
		parsedData[j].Az *= 9.81 / offset[2]
		if err != nil {
			return nil, err
		}

	}

	//smooth the acceleration
	for j := 0; j < len(parsedData); j++ {
		interval := 50
		if j < interval {
			sumx := 0.0
			sumy := 0.0
			sumz := 0.0

			for k := 0; k < j+interval; k++ {
				sumx += parsedData[k].Ax
				sumy += parsedData[k].Ay
				sumz += parsedData[k].Az
			}

			correctedX[j] = sumx / float64(j+interval)
			correctedY[j] = sumy / float64(j+interval)
			correctedZ[j] = sumz / float64(j+interval)

		} else if j >= len(parsedData)-interval {
			sumx := 0.0
			sumy := 0.0
			sumz := 0.0

			for k := j - interval; k < len(parsedData); k++ {
				sumx += parsedData[k].Ax
				sumy += parsedData[k].Ay
				sumz += parsedData[k].Az
			}

			correctedX[j] = sumx / float64(len(parsedData)-j+interval)
			correctedY[j] = sumy / float64(len(parsedData)-j+interval)
			correctedZ[j] = sumz / float64(len(parsedData)-j+interval)
		} else {
			sumx := 0.0
			sumy := 0.0
			sumz := 0.0

			for k := j - interval; k < j+interval; k++ {
				sumx += parsedData[k].Ax
				sumy += parsedData[k].Ay
				sumz += parsedData[k].Az
			}

			correctedX[j] = sumx / float64(interval*2)
			correctedY[j] = sumy / float64(interval*2)
			correctedZ[j] = sumz / float64(interval*2)
		}
	}

	//copy the corrected data
	for j := 0; j < len(parsedData); j++ {
		parsedData[j].Ax = correctedX[j]
		parsedData[j].Ay = correctedY[j]
		parsedData[j].Az = correctedZ[j]

		//calculate speed and location
		if j == 0 {
			parsedData[j].DeltaT = 0

			parsedData[j].Vx = parsedData[j].Ax * parsedData[j].DeltaT
			parsedData[j].Vy = parsedData[j].Ay * parsedData[j].DeltaT
			parsedData[j].Vz = parsedData[j].Az * parsedData[j].DeltaT

			parsedData[j].Sx = 0.5 * parsedData[j].Ax * parsedData[j].DeltaT * parsedData[j].DeltaT
			parsedData[j].Sy = 0.5 * parsedData[j].Ay * parsedData[j].DeltaT * parsedData[j].DeltaT
			parsedData[j].Sz = 0.5 * parsedData[j].Az * parsedData[j].DeltaT * parsedData[j].DeltaT

			parsedData[j].ErrV = deltaA * parsedData[j].DeltaT
			parsedData[j].ErrS = 0.5 * deltaA * parsedData[j].DeltaT * parsedData[j].DeltaT
		} else {
			parsedData[j].DeltaT = parsedData[j].Time - parsedData[j-1].Time

			parsedData[j].Vx = parsedData[j-1].Vx + parsedData[j].Ax*parsedData[j].DeltaT
			parsedData[j].Vy = parsedData[j-1].Vy + parsedData[j].Ay*parsedData[j].DeltaT
			parsedData[j].Vz = parsedData[j-1].Vz + parsedData[j].Az*parsedData[j].DeltaT

			parsedData[j].Sx = parsedData[j-1].Sx + parsedData[j-1].Vx*parsedData[j].DeltaT + 0.5*parsedData[j].Ax*parsedData[j].DeltaT*parsedData[j].DeltaT
			parsedData[j].Sy = parsedData[j-1].Sy + parsedData[j-1].Vy*parsedData[j].DeltaT + 0.5*parsedData[j].Ay*parsedData[j].DeltaT*parsedData[j].DeltaT
			parsedData[j].Sz = parsedData[j-1].Sz + parsedData[j-1].Vz*parsedData[j].DeltaT + 0.5*parsedData[j].Az*parsedData[j].DeltaT*parsedData[j].DeltaT

			parsedData[j].ErrV = parsedData[j-1].ErrV + deltaA*parsedData[j].DeltaT
			parsedData[j].ErrS = parsedData[j-1].ErrS + parsedData[j-1].ErrV*parsedData[j].DeltaT + 0.5*deltaA*parsedData[j].DeltaT*parsedData[j].DeltaT

		}

		parsedData[j].ErrA = deltaA
		parsedData[j].Ages = vectorVal(parsedData[j].Ax, parsedData[j].Ay, 0)
		parsedData[j].Vges = vectorVal(parsedData[j].Vx, parsedData[j].Vy, 0)
		parsedData[j].Sges = vectorVal(parsedData[j].Sx, parsedData[j].Sy, 0)
	}

	return parsedData, nil
}

package main

import (
	"log"
	"math"
	"os"
	"os/exec"
	"strconv"
)

var radius = 6378100.0

func main() {

	experiment := 2
	graph(experiment, "calibration.csv", 19.60120672, 407.0203113)

	py, _ := exec.LookPath("python3")
	graph := exec.Command(py, strconv.Itoa(experiment)+".graph.py")
	graph.Stdout = os.Stdout
	graph.Stderr = os.Stderr
	_ = graph.Run()

	log.Println("a graph closed")

}

func graph(experimentNumer int, calibrationFile string, startingTime float64, endTime float64) {

	file1 := "Accelerometer_"
	file2 := "Location_"

	dataFileAccel := file1 + strconv.Itoa(experimentNumer) + ".csv"
	dataFileGPS := file2 + strconv.Itoa(experimentNumer) + ".csv"

	//delte old files
	files, err := os.Create(strconv.Itoa(experimentNumer) + ".graph.py") // For read access.
	if err != nil {
		log.Fatal(err)
	}
	_, err = files.WriteString("")
	if err != nil {
		log.Fatal(err)
	}

	files, err = os.Create(dataFileAccel + ".data.json") // For read access.
	if err != nil {
		log.Fatal(err)
	}
	_, err = files.WriteString("")
	if err != nil {
		log.Fatal(err)
	}

	files, err = os.Create(dataFileGPS + ".data.json") // For read access.
	if err != nil {
		log.Fatal(err)
	}
	_, err = files.WriteString("")
	if err != nil {
		log.Fatal(err)
	}

	//read the csv data into an array
	recordsA, err := readCSVFile(dataFileAccel)
	if err != nil {
		log.Fatal(err)
	}

	recordsG, err := readCSVFile(dataFileGPS, 8)
	if err != nil {
		log.Fatal(err)
	}

	//get the correction factor
	korrektur, err := calibrateAccel(calibrationFile)

	//parse and process the data into an point array
	dataAccel, err := parseCSVDataAccel(recordsA, korrektur, startingTime, endTime)
	if err != nil {
		log.Fatal(err)
	}

	dataGPS, err := parseCSVDataGPS(recordsG, startingTime, endTime)
	if err != nil {
		log.Fatal(err)
	}

	//calculate and print the average speed
	sum := 0.0
	for i := range dataAccel {
		sum += math.Abs(dataAccel[i].Vy)
	}
	sum /= float64(len(dataAccel))

	sum2 := 0.0
	for i := range dataGPS {
		sum2 += math.Abs(dataGPS[i].Vges)
	}
	sum2 /= float64(len(dataGPS))

	sum3 := 0.0
	pM := 0.0
	aM := 0.0
	dp := 0.0
	da := 0.0
	for i := range dataGPS {
		sum3 += math.Abs(dataGPS[i].Power)
		if dataGPS[i].Power > pM {
			pM = dataGPS[i].Power
			dp = dataGPS[i].ErrP
		}
		if dataGPS[i].Ages > aM {
			aM = dataGPS[i].Ages
			da = dataGPS[i].ErrA
		}
	}
	sum3 /= float64(len(dataGPS)) * 1000.0

	log.Println("Auswertung der Beschleunigung:")
	log.Printf("%d Werte wurden ausgewertet.", len(dataAccel))
	log.Printf("Die Gesamtsrecke beträgt %f und die Gesamtzeit beträgt %f", dataAccel[len(dataAccel)-1].Sges, dataAccel[len(dataAccel)-1].Time)
	log.Printf("Die Durchschnittsgeschwindigkeit 1 beträgt %f m/s", sum)
	log.Printf("Die Durchschnittsgeschwindigkeit 2 beträgt %f m/s", dataAccel[len(dataAccel)-1].Sges/dataAccel[len(dataAccel)-1].Time)
	log.Printf("Die Mittlere Zeit zwischen zwei Messpunkten beträgt %f s", dataAccel[len(dataAccel)-1].Time/float64(len(dataAccel)))
	log.Printf("Im Mittel gibt es %f Messpunkte pro Sekunde", 1/(dataAccel[len(dataAccel)-1].Time/float64(len(dataAccel))))
	log.Printf("Delta VN beträgt:%f m/s", dataAccel[len(dataAccel)-1].ErrV)
	log.Printf("Delta SN beträgt:%f m", dataAccel[len(dataAccel)-1].ErrS)

	log.Printf("Die Streckenlänge liegt zwischen %f m und %f m.\n\n", dataAccel[len(dataAccel)-1].Sges-dataAccel[len(dataAccel)-1].ErrS, dataAccel[len(dataAccel)-1].Sges+dataAccel[len(dataAccel)-1].ErrS)

	log.Println("Auswertung des Standorts:")
	log.Printf("%d Werte wurden ausgewertet.", len(dataGPS))
	log.Printf("Die Gesamtsrecke beträgt %f und die Gesamtzeit beträgt %f", dataGPS[len(dataGPS)-1].Sges, dataGPS[len(dataGPS)-1].Time)
	log.Printf("Die Durchschnittsgeschwindigkeit 1 beträgt %f m/s", sum2)
	log.Printf("Die Durchschnittsgeschwindigkeit 2 beträgt %f m/s", dataGPS[len(dataGPS)-1].Sges/dataGPS[len(dataGPS)-1].Time)
	log.Printf("Die Mittlere Zeit zwischen zwei Messpunkten beträgt %f s", dataGPS[len(dataGPS)-1].Time/float64(len(dataGPS)))
	log.Printf("Im Mittel gibt es %f Messpunkte pro Sekunde", 1/(dataGPS[len(dataGPS)-1].Time/float64(len(dataGPS))))
	log.Printf("Delta VN beträgt:%f m/s", dataGPS[len(dataGPS)-1].ErrV)
	log.Printf("Delta SN beträgt:%f m\n\n", dataGPS[len(dataGPS)-1].ErrS)

	log.Printf("Die verrichtete Arbeit beträgt:%f J", dataGPS[len(dataGPS)-1].Work)
	log.Printf("Die maximale Beschleunigung beträgt <%f+-%f>m/s² und die maximale Leistung beträgt <%f+-%f>kW", aM, da, pM/1000, dp/1000)
	log.Printf("Die Durchschnittsleistung beträgt %f kW\n\n", sum3)

	log.Printf("Die Streckenlänge liegt zwischen %f m und %f m.\n\n", dataGPS[len(dataGPS)-1].Sges-dataGPS[len(dataGPS)-1].ErrS, dataGPS[len(dataGPS)-1].Sges+dataGPS[len(dataGPS)-1].ErrS)

	//create the python graph
	createPythonGraph(dataAccel, dataFileAccel, dataGPS, dataFileGPS, strconv.Itoa(experimentNumer)+".graph.py")

}

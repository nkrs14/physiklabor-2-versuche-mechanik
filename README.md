# Readme

Zum ausführen des Programms werden [pyhton3](https://www.python.org/), [python3-pip](https://pip.pypa.io/en/stable/installing/), [go](https://golang.org/) und [dep](https://github.com/golang/dep) benötigt.

Für die folgenden Befehle müssen die Dateien in einem unterordner von `go/src` sein z.B. in `go/src/labor_4` und eine Komandozeile bzw. ein Terminal in diesem Ordner geöffnet werden.
Dann müssen die python dependencies mit `python3 -m pip install -r requirements.txt` installiert werden.
Daraufhin müssen mit `dep ensure` die go dependencies installiert werden.
Nun kann mit `go run .` der code kompiliert und ausgefühert werden.

Protokoll 4 und Protokoll 5 sind die Ordner zu dem Code (ins besondere Protokoll 5).
Alle Details zum versuch stehen in den Versuchsprotokollen.

Protokoll BAT ist ein Versuchsprotokoll für einen anderen Versuch.

**All diese Protokolle sind keine Vorzeigeexemplare, das wichtigste an diesem Repository ist der Code!**
Der code kann als Beispiel zur Auswertung von Versuchen mit einem Programm verwendet werden.

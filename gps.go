package main

import (
	"errors"
	"math"
)

type gpsPoint struct {
	Time   float64 `json:"time"`
	Lat    float64 `json:"lat"`
	Long   float64 `json:"long"`
	Height float64 `json:"height"`
	Vel    float64 `json:"vel"`
	Dir    float64 `json:"dir"`
	HAcc   float64 `json:"hAcc"`
	VAcc   float64 `json:"vAcc"`
	Sx     float64 `json:"sx"`
	Sy     float64 `json:"sy"`
	Sz     float64 `json:"sz"`
	Sges   float64 `json:"sges"`
	Ay     float64
}

var deltaT = 0.01
var mass = 119400.0
var deltaM = 1000.0

func parseCSVDataGPS(data [][]string, startingTime float64, endTime float64) (parsedData []point, err error) {
	var dat []gpsPoint
	var firstIndex int
	var lastIndex int

	//find out where the wanted data is
	for i := 1; i < len(data); i++ {
		time, err := parseFloat(data[i][0])
		if err != nil {
			return nil, err
		}

		if time < startingTime {
			continue
		} else if firstIndex == 0 {
			firstIndex = i
		} else if time > endTime {
			lastIndex = i
			parsedData = make([]point, lastIndex-firstIndex)

			dat = make([]gpsPoint, lastIndex-firstIndex)
			break
		}
	}

	//if no start or end found exit
	if firstIndex == 0 || lastIndex == 0 {
		return nil, errors.New("index not set correctly")
	}

	//parse the data into dat
	for j := 0; j < len(parsedData); j++ {

		dat[j].Time, err = parseFloat(data[j+firstIndex][0])
		if err != nil {
			return nil, err
		}
		dat[j].Time -= startingTime
		parsedData[j].Time = dat[j].Time

		dat[j].Lat, err = parseFloat(data[j+firstIndex][1])
		if err != nil {
			return nil, err
		}
		dat[j].Lat *= math.Pi / 180.0

		dat[j].Long, err = parseFloat(data[j+firstIndex][2])
		if err != nil {
			return nil, err
		}
		dat[j].Long *= math.Pi / 180.0

		dat[j].Height, err = parseFloat(data[j+firstIndex][3])
		if err != nil {
			return nil, err
		}

		dat[j].Vel, err = parseFloat(data[j+firstIndex][4])
		if err != nil {
			return nil, err
		}

		dat[j].Dir, err = parseFloat(data[j+firstIndex][5])
		if err != nil {
			return nil, err
		}

		dat[j].HAcc, err = parseFloat(data[j+firstIndex][6])
		if err != nil {
			return nil, err
		}

		dat[j].VAcc, err = parseFloat(data[j+firstIndex][7])
		if err != nil {
			return nil, err
		}

		dat[j].Sx = math.Sin(dat[j].Lat) * math.Cos(dat[j].Long)
		dat[j].Sy = math.Sin(dat[j].Lat) * math.Sin(dat[j].Long)
		dat[j].Sz = math.Cos(dat[j].Lat)

		if j == 0 {
			dat[j].Sges = 0
		} else {
			dat[j].Sges = vectorVal(dat[j].Sx-dat[j-1].Sx, dat[j].Sy-dat[j-1].Sy, dat[j].Sz-dat[j-1].Sz)
		}

	}

	//smooth the location
	for j := 0; j < len(parsedData); j++ {
		interval := 3
		if j < interval {
			sums := 0.0
			sumh := 0.0
			sumh1 := 0.0
			sumv := 0.0

			for k := 0; k < j+interval; k++ {
				sums += dat[k].Sges
				sumh += dat[k].Height
				sumv += dat[k].Vel
				sumh1 += dat[k].HAcc
			}

			sumh /= float64(j + interval)

			parsedData[j].Sges = (radius + sumh) * sums / float64(j+interval)
			parsedData[j].Height = sumh
			parsedData[j].ErrH = sumh1 / float64(j+interval)
			parsedData[j].Vy = sumv / float64(j+interval)

			//parsedData[j].Sges = radius * sums / float64(j+interval)

		} else if j >= len(parsedData)-interval {
			sums := 0.0
			sumh := 0.0
			sumh1 := 0.0
			sumv := 0.0

			for k := j - interval; k < len(parsedData); k++ {
				sums += dat[k].Sges
				sumh += dat[k].Height
				sumv += dat[k].Vel
				sumh1 += dat[k].HAcc
			}

			sumh /= float64(len(parsedData) - j + interval)

			parsedData[j].Sges = (radius + sumh) * sums / float64(len(parsedData)-j+interval)
			parsedData[j].Height = sumh
			parsedData[j].ErrH = sumh1 / float64(len(parsedData)-j+interval)
			parsedData[j].Vy = sumv / float64(len(parsedData)-j+interval)
		} else {
			sums := 0.0
			sumh := 0.0
			sumh1 := 0.0
			sumv := 0.0

			for k := j - interval; k < j+interval; k++ {
				sums += dat[k].Sges
				sumh += dat[k].Height
				sumv += dat[k].Vel
				sumh1 += dat[k].HAcc
			}

			sumh /= float64(interval * 2)

			parsedData[j].Sges = (radius + sumh) * sums / float64(interval*2)
			parsedData[j].Height = sumh
			parsedData[j].ErrH = sumh1 / float64(interval*2)
			parsedData[j].Vy = sumv / float64(interval*2)
		}

		parsedData[j].Work = 0
		parsedData[j].ErrW = 0
		parsedData[j].ErrP = 0

		if j == 0 {
			parsedData[j].DeltaT = dat[j].Time
			parsedData[j].Vges = 0
			parsedData[j].Ages = 0
			parsedData[j].Power = 0

			continue
		}

		dH := parsedData[j].Height - parsedData[j-1].Height
		if dH > 0 {
			parsedData[j].Work += mass * 9.81 * dH
		}

		parsedData[j].DeltaT = dat[j].Time - dat[j-1].Time
		parsedData[j].Vges = parsedData[j].Sges / parsedData[j].DeltaT
		parsedData[j].Ages = (parsedData[j].Vges - parsedData[j-1].Vges) / parsedData[j].DeltaT
		dat[j].Ay = (parsedData[j].Vy - parsedData[j-1].Vy) / parsedData[j].DeltaT

		if parsedData[j].Ages > 0 {
			parsedData[j].Work += mass * parsedData[j].Ages * parsedData[j].Sges
		}

		parsedData[j].Power = parsedData[j].Work / parsedData[j].DeltaT

	}

	correctedV := make([]float64, len(parsedData))
	correctedA := make([]float64, len(parsedData))
	correctedP := make([]float64, len(parsedData))

	for j := 0; j < len(parsedData); j++ {
		interval := 5
		if j < interval {
			sumV := 0.0
			sumA := 0.0
			sumP := 0.0
			sumErrS := 0.0
			sumAy := 0.0

			for k := 1; k < j+interval; k++ {
				sumV += parsedData[k].Vges
				sumA += parsedData[k].Ages
				sumP += parsedData[k].Power
				sumErrS += pyt(dat[k].HAcc, dat[k].VAcc)
				sumAy += dat[k].Ay
			}

			correctedV[j] = sumV / float64(j+interval)
			correctedA[j] = sumA / float64(j+interval)
			correctedP[j] = sumP / float64(j+interval)
			parsedData[j].ErrS = sumErrS / float64(j+interval)
			parsedData[j].Ay = sumAy / float64(j+interval)

		} else if j >= len(parsedData)-interval {
			sumV := 0.0
			sumA := 0.0
			sumP := 0.0
			sumErrS := 0.0
			sumAy := 0.0

			for k := j - interval; k < len(parsedData); k++ {
				sumV += parsedData[k].Vges
				sumA += parsedData[k].Ages
				sumP += parsedData[k].Power
				sumErrS += pyt(dat[k].HAcc, dat[k].VAcc)
				sumAy += dat[k].Ay
			}

			correctedV[j] = sumV / float64(len(parsedData)-j+interval)
			correctedA[j] = sumA / float64(len(parsedData)-j+interval)
			correctedP[j] = sumP / float64(len(parsedData)-j+interval)
			parsedData[j].ErrS = sumErrS / float64(len(parsedData)-j+interval)
			parsedData[j].Ay = sumAy / float64(len(parsedData)-j+interval)

		} else {
			sumV := 0.0
			sumA := 0.0
			sumP := 0.0
			sumErrS := 0.0
			sumAy := 0.0

			for k := j - interval; k < j+interval; k++ {
				sumV += parsedData[k].Vges
				sumA += parsedData[k].Ages
				sumP += parsedData[k].Power
				sumErrS += pyt(dat[k].HAcc, dat[k].VAcc)
				sumAy += dat[k].Ay
			}

			correctedV[j] = sumV / float64(interval*2)
			correctedA[j] = sumA / float64(interval*2)
			correctedP[j] = sumP / float64(interval*2)
			parsedData[j].ErrS = sumErrS / float64(interval*2)
			parsedData[j].Ay = sumAy / float64(interval*2)
		}

	}

	for j := 0; j < len(parsedData); j++ {
		parsedData[j].Vges = correctedV[j]
		parsedData[j].Ages = correctedA[j]
		parsedData[j].Power = correctedP[j]
		parsedData[j].ErrH /= 5

		if j == 0 {
			parsedData[j].ErrV = (parsedData[j].ErrS) / (10 * parsedData[j].DeltaT)

			if math.IsInf(parsedData[j].ErrV, 0) {
				parsedData[j].ErrV = 0
			}

			parsedData[j].ErrA = (parsedData[j].ErrV) / (20 * parsedData[j].DeltaT)

			if math.IsInf(parsedData[j].ErrA, 0) || math.IsNaN(parsedData[j].ErrA) {
				parsedData[j].ErrA = 0
			}

			continue
		}

		parsedData[j].ErrV = (parsedData[j].ErrS + parsedData[j-1].ErrS) / (10 * parsedData[j].DeltaT)

		if math.IsInf(parsedData[j].ErrV, 0) {
			parsedData[j].ErrV = 0
		}

		parsedData[j].ErrA = (parsedData[j].ErrV + parsedData[j-1].ErrV) / (20 * parsedData[j].DeltaT)

		if math.IsInf(parsedData[j].ErrA, 0) || math.IsNaN(parsedData[j].ErrA) {
			parsedData[j].ErrA = 0
		}

		dm := deltaM / mass
		da := parsedData[j].ErrA / (5 * parsedData[j].Ages)
		ds := parsedData[j].ErrS / (5 * parsedData[j].Sges)

		if math.IsInf(da, 0) {
			da = 0
		}
		if math.IsInf(ds, 0) {
			ds = 0
		}

		parsedData[j].ErrW += mass * parsedData[j].Ages * parsedData[j].Sges * (dm + da + ds)

		dH := parsedData[j].Height - parsedData[j-1].Height
		if dH > 0 {
			parsedData[j].ErrW += 9.81 * (dH*deltaM + 0.1*mass*parsedData[j].ErrH)
		}

		parsedData[j].ErrP = parsedData[j].ErrW / parsedData[j].DeltaT

	}

	for j := 1; j < len(parsedData); j++ {
		parsedData[j].Sges += parsedData[j-1].Sges
		parsedData[j].ErrS /= 5
		parsedData[j].ErrS += parsedData[j-1].ErrS
		parsedData[j].Work += parsedData[j-1].Work
	}

	return parsedData, nil
}

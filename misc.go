package main

import (
	"encoding/csv"
	"errors"
	"log"
	"math"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

type point struct {
	Time   float64 `json:"time"`
	Ax     float64 `json:"ax"`
	Ay     float64 `json:"ay"`
	Az     float64 `json:"az"`
	Vx     float64 `json:"vx"`
	Vy     float64 `json:"vy"`
	Vz     float64 `json:"vz"`
	Sx     float64 `json:"sx"`
	Sy     float64 `json:"sy"`
	Sz     float64 `json:"sz"`
	Ages   float64 `json:"ages"`
	Vges   float64 `json:"vges"`
	Sges   float64 `json:"sges"`
	DeltaT float64 `json:"deltaT"`
	ErrA   float64 `json:"errA"`
	ErrV   float64 `json:"errV"`
	ErrS   float64 `json:"errS"`
	Height float64 `json:"height"`
	Work   float64 `json:"work"`
	Power  float64 `json:"power"`
	ErrW   float64 `json:"errW"`
	ErrP   float64 `json:"errP"`
	ErrH   float64 `json:"errH"`
}

func average(data []float64) (avg float64, err error) {
	avg = 0.0

	if len(data) == 0 {
		return avg, errors.New("got empty array")
	}

	for i := range data {
		avg += data[i]
	}
	avg /= float64(len(data))

	return avg, nil
}

func absAverage(data []float64) (avg float64, err error) {
	avg = 0.0

	if len(data) == 0 {
		return avg, errors.New("got empty array")
	}

	for i := range data {
		avg += math.Abs(data[i])
	}
	avg /= float64(len(data))

	return avg, nil
}

func vectorVal(x, y, z float64) float64 {
	tmp := x * x
	tmp += y * y
	tmp += z * z
	return math.Sqrt(tmp)
}

func pyt(x, y float64) float64 {
	tmp := x * x
	tmp += y * y
	return math.Sqrt(tmp)
}

func readCSVFile(file string, leng ...int) (record [][]string, err error) {

	size := 4
	if len(leng) != 0 {
		size = leng[0]
	}

	file = filepath.Clean(file)
	files, err := os.Open(file) // For read access.
	if err != nil {
		log.Fatal(err)
	}
	data := make([]byte, 1000000000)
	count, err := files.Read(data)
	if err != nil {
		log.Fatal(err)
	}

	in := string(data[0:count])

	r := csv.NewReader(strings.NewReader(in))
	r.FieldsPerRecord = size
	r.TrimLeadingSpace = true

	return r.ReadAll()
}

func parseFloat(str string) (float64, error) {
	val, err := strconv.ParseFloat(str, 64)
	if err == nil {
		return val, nil
	}
	//Some number may be seperated by comma, for example, 23,120,123, so remove the comma firstly
	str = strings.Replace(str, ",", "", -1)

	//Some number is specifed in scientific notation
	pos := strings.IndexAny(str, "eE")
	if pos < 0 {
		return strconv.ParseFloat(str, 64)
	}

	var baseVal float64
	var expVal int64

	baseStr := str[0:pos]
	baseVal, err = strconv.ParseFloat(baseStr, 64)
	if err != nil {
		return 0, err
	}

	expStr := str[(pos + 1):]
	expVal, err = strconv.ParseInt(expStr, 10, 64)
	if err != nil {
		return 0, err
	}

	return baseVal * math.Pow10(int(expVal)), nil
}
